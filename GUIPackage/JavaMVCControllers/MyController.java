package GUIPackage.JavaMVCControllers;

import GUIPackage.JavaMVCViews.*;
import GUIPackage.JavaMVCModels.*;

public class MyController {
    private boolean receiving = false;
    private MyModel model;
    private MyForm view;

    public MyController() {
        model = new MyModel();
    }

    public void startApplication() {
        view = new MyForm(this);
        view.setVisible(true);
    }
}
