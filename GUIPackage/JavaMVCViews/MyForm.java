package GUIPackage.JavaMVCViews;

import GUIPackage.JavaMVCControllers.MyController;

import javax.swing.*;
import java.awt.*;

public class MyForm extends javax.swing.JFrame {
    private MyController controller;
    private JLabel myLabel;

    public MyForm(MyController controller) throws HeadlessException {
        this.controller = controller;
        init();
    }

    private void init()  {
        myLabel.setText("Hier könnte ihre Werbung stehen.");

        GroupLayout layout = new GroupLayout(getContentPane());

        GroupLayout.SequentialGroup sequentialGroup =
                layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                                .addComponent(myLabel, GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE))
                                        .addContainerGap()));

        GroupLayout.ParallelGroup horizontalGroup =
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(sequentialGroup)
                        .addGroup(layout.createSequentialGroup());

        GroupLayout.ParallelGroup verticalGroup =
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(myLabel));

        layout.setHorizontalGroup(horizontalGroup);
        layout.setVerticalGroup(verticalGroup);

        getContentPane().setLayout(layout);

        pack();
    }
}
