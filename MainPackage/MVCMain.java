package MainPackage;

import GUIPackage.JavaMVCControllers.*;

public class MVCMain {
    public static void main(String[] args) {
        MyController controller = new MyController();
        controller.startApplication();
    }
}
